<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic\Tests;

use dic\Container;

/**
 * Class ComplexUseCaseTest
 *
 * @author Jose Luis Poveda <jlpoveda@gmail.com>
 */
class ComplexUseCaseTest extends \PHPUnit_Framework_TestCase {

    public function testUseCase()
    {
        $serviceContainer = new Container();
        $serviceContainer
            ->registerService('d', 'dic\Tests\D')
            ->addArgument('dummy_text');

        $serviceContainer
            ->registerService('c', 'dic\Tests\C');

        $serviceContainer
            ->registerService('b', 'dic\Tests\B')
            ->addArgument($serviceContainer->getService('c'))
            ->addArgument($serviceContainer->getService('d'));

        $serviceContainer
            ->registerService('a', 'dic\Tests\A');

        $serviceContainer
            ->getService('a')
            ->addArgument($serviceContainer->getService('b'));

        /** @var A $a */
        $a = $serviceContainer->get('a');

        $this->assertInstanceOf('dic\Tests\B', $a->getB());
        $this->assertInstanceOf('dic\Tests\C', $a->getB()->getC());

        $this->assertEquals('C Class', $a->getB()->getC());
        $this->assertEquals('dummy_text', $a->getB()->getD()->text());
    }
}

Class A
{
    protected $b;

    public function __construct(B $b)
    {
        $this->b = $b;
    }

    public function getB()
    {
        return $this->b;
    }
}

Class B
{
    protected $c;
    protected $d;

    public function __construct(C $c, D $d)
    {
        $this->c = $c;
        $this->d = $d;
    }

    public function getC()
    {
        return $this->c;
    }

    public function getD()
    {
        return $this->d;
    }
}

Class C
{
    public function __toString()
    {
        return 'C Class';
    }
}

Class D
{
    protected $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function text()
    {
        return $this->text;
    }
}

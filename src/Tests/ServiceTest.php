<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic\Tests;

use dic\Service;

/**
 * Class ServiceTest
 *
 * @author Jose Luis Poveda <jlpoveda@gmail.com>
 */
class ServiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers dic\Service::__construct
     * @covers dic\Service::addArgument
     * @covers dic\Service::call
     * @covers dic\Exception\ClassNotExistsException
     */
    public function testCallToANonExistentClass()
    {
        $service = new Service('Foo');

        $service->addArgument('Bar');

        try
        {
            $service->call();
        } catch(\Exception $e) {
            $this->assertInstanceOf('dic\Exception\ClassNotExistsException', $e);
        }
    }

    /**
     * @covers dic\Service::__construct
     * @covers dic\Service::addArgument
     * @covers dic\Service::call
     */
    public function testCallToStdClass()
    {
        $service = new Service('\StdClass');
        $service->addArgument('Foo');

        $service->call();
    }
}
 
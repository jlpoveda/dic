<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic\Tests;

use dic\Container;

/**
 * Class ContainerTest
 *
 * @author Jose Luis Poveda <jlpoveda@gmail.com>
 */
class ContainerTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers dic\Container::has
     * @covers dic\Container::registerService
     * @covers dic\Container::setService
     * @covers dic\Container::normalize
     */
    public function testRegisterService()
    {
        $container = new Container();
        $container->registerService('Mailer', 'Foo\Mailer');
        $this->assertTrue($container->has('mailer'));

        $container->registerService('Printer', 'Foo\Printer');
        $this->assertTrue($container->has('mailer'));
        $this->assertTrue($container->has('printer'));
    }

    /**
     * @covers dic\Container::get
     * @covers dic\Container::getService
     * @covers dic\Exception\ServiceNotRegisteredException
     */
    public function testHasWithException()
    {
        $container = new Container();
        try
        {
            $container->get('dummy');
        } catch(\Exception $e) {
            $this->assertInstanceOf('dic\Exception\ServiceNotRegisteredException', $e);
        }
    }

    /**
     * @covers dic\Container::get
     * @covers dic\Container::registerService
     * @covers dic\Container::setService
     * @covers dic\Container::normalize
     */
    public function testGetWithException()
    {
        $container = new Container();
        $container->registerService('Mailer', 'Foo\Mailer');
        try
        {
            $container->get('mailer');
        } catch (\Exception $e) {
            $this->assertInstanceOf('\Exception', $e);
        }
    }

}
 
<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic\Tests;

use dic\Container;

/**
 * Class UseCaseTest
 *
 * @author Jose Luis Poveda <jlpoveda@gmail.com>
 */
class UseCaseTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers dic\Container::get
     * @covers dic\Container::registerService
     * @covers dic\Container::getService
     * @covers dic\Service::__construct
     * @covers dic\Service::addArgument
     * @covers dic\Service::call
     */
    public function testContainer()
    {
        $serviceContainer = new Container();
        $serviceContainer
            ->registerService('mailer', 'dic\Tests\Mailer')
            ->addArgument('sendmail');

        $mailer = $serviceContainer->get('mailer');

        $this->assertInstanceOf('dic\Tests\Mailer', $mailer);
        $this->assertEquals('sendmail', $mailer->getSendMethod());
    }

    /**
     * @covers dic\Container::get
     * @covers dic\Container::registerService
     * @covers dic\Container::getService
     * @covers dic\Service::__construct
     * @covers dic\Service::addArgument
     * @covers dic\Service::call
     */
    public function testObjectWithoutConstructor()
    {
        $serviceContainer = new Container();
        $serviceContainer
            ->registerService('dummy', 'dic\Tests\ObjectWithoutConstructor');

        $dummy = $serviceContainer->get('dummy');

        $this->assertInstanceOf('dic\Tests\ObjectWithoutConstructor', $dummy);
        $this->assertEquals('hello', $dummy->saySomething('hello'));
    }

    public function testInjectObjectInObject()
    {
        $serviceContainer = new Container();
        $serviceContainer
            ->registerService('sender', 'dic\Tests\Sender');

        $serviceContainer
            ->registerService('mailer', 'dic\Tests\SmartMailer')
            ->addArgument($serviceContainer->getService('sender'));

        $mailer = $serviceContainer->get('mailer');

        $this->assertInstanceOf('dic\Tests\SmartMailer', $mailer);
        $this->assertEquals('I am a Sender', $mailer->viewSender());
    }
}

class Mailer
{
    protected $mailMethod;

    public function __construct($sendMethod)
    {
        $this->sendMethod = $sendMethod;
    }

    public function getSendMethod()
    {
        return $this->sendMethod;
    }
}

class ObjectWithoutConstructor
{
    public function saySomething($text)
    {
        return $text;
    }
}

class SmartMailer
{
    /** @var  Sender */
    private $sender;

    public function __construct(Sender $sender)
    {
        $this->sender = $sender;
    }

    public function viewSender()
    {
        return $this->sender;
    }
}

class Sender
{
    public function __toString()
    {
        return 'I am a Sender';
    }
}
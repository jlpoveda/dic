<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic;

/**
 * Interface ServiceInterface
 *
 * @author Jose Luis Poveda <jlpoveda@gmail.com>
 */
interface ServiceInterface
{
    /**
     * Add Argument to the service
     *
     * @param $value
     *
     * @return $this
     */
    public function addArgument($value);

    /**
     * Construct de service with its arguments and save it in the $service property like if it was a cache to
     * future calls faster.
     *
     * @param bool $newInstance
     *
     * @return object
     *
     * @throws ClassNotExistsException
     */
    public function call($newInstance);
}
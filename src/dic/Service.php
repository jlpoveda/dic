<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic;

use dic\Exception\ClassNotExistsException;

/**
 * Class Service
 *
 * @author Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * @package dic
 */
class Service implements ServiceInterface
{
    protected $class;
    protected $arguments = array();
    protected $service = null;

    /**
     * Constructor
     *
     * @param string $className The name of the service
     */
    public function __construct($className)
    {
        $this->class = $className;

    }

    /**
     * Add Argument to the service
     *
     * @param $value
     *
     * @return $this
     */
    public function addArgument($value)
    {
        $this->arguments[] = $value;

        return $this;
    }

    /**
     * Construct de service with its arguments and save it in the $service property like if it was a cache to
     * future calls faster.
     *
     * @param bool $newInstance
     *
     * @return object
     *
     * @throws ClassNotExistsException
     */
    public function call($newInstance = false)
    {
        if(!is_null($this->service) && false === $newInstance) {
            return $this->service;
        }

        try
        {
            $r = new \ReflectionClass($this->class);
        } catch(\Exception $e) {
            throw new ClassNotExistsException(sprintf("Class %s not exits", $this->class));
        }

        $new_arguments = array();
        foreach($this->arguments as $argument) {
            if($argument instanceof \dic\Service) {
                $argument = $argument->call($newInstance);
            }
            $new_arguments[] = $argument;
        }
        $this->arguments = $new_arguments;

        return $this->service = null === $r->getConstructor() ? $r->newInstance() : $r->newInstanceArgs($this->arguments);
    }
}
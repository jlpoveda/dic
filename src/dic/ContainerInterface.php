<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic;

use Interop\Container\ContainerInterface as ContainerInteropInterface;

/**
 * Extends Container Interoperability to achieve interopererability.
 *
 * Interface ContainerInterface
 * @package dic
 */
interface ContainerInterface extends ContainerInteropInterface
{
    public function registerService($id, $service);
}
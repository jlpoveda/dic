<?php
namespace dic\Exception;

class ClassNotExistsException extends \InvalidArgumentException
{
}
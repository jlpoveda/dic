<?php

/*
 * This file is part of the dic library.
 *
 * (c) Jose Luis Poveda <jlpoveda@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dic;

use dic\Exception\ServiceNotRegisteredException;

/**
 * Class dic
 *
 * Implements Container Interoperability to achieve interopererability.
 *
 * @author Jose Luis Poveda <jlpoveda@gmail.com>
 */
class Container implements ContainerInterface
{
    protected $services = array();

    /**
     * @param string $id
     * @param bool   $newInstance
     *
     * @return object
     *
     * @throws ServiceNotRegisteredException
     */
    public function get($id, $newInstance = false)
    {
        $id = $this->normalize($id);
        if(!$this->has($id)) {
            throw new ServiceNotRegisteredException(sprintf("Service $id not found", $id));
        }

        return $this->getService($id)->call($newInstance);
    }

    /**
     * Checks if a service has been defined in the container
     *
     * @param string $id
     * @return bool
     */
    public function has($id)
    {
        return array_key_exists($this->normalize($id), $this->services);
    }

    /**
     * Register the service in the container
     *
     * @param $id
     * @param $className
     *
     * @return Service
     */
    public function registerService($id, $className)
    {
        return $this->setService($id, new Service($className));
    }

    /**
     * Sets the service in the container
     *
     * @param $id
     * @param Service $service
     *
     * @return Service
     */
    public function setService($id, Service $service)
    {
        return $this->services[$this->normalize($id)] = $service;
    }

    /**
     * Returns an specific service
     *
     * @param $id
     * @return mixed
     */
    public function getService($id) {
        return $this->services[$this->normalize($id)];
    }

    protected function normalize($id)
    {
        return strtolower($id);
    }
}

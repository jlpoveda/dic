# dic #

__dic__ is a very basic Dependency Injection Container.

### Basic Usage

```php
use dic\Container;

$serviceContainer = new Container();
$serviceContainer
->registerService('mailer', 'dic\Tests\Mailer')
->addArgument('sendmail');

$mailer = $serviceContainer->get('mailer');

/* ... */

class Mailer
{
    protected $mailMethod;

    public function __construct($sendMethod)
    {
        $this->sendMethod = $sendMethod;
    }
}
```

You can inject __services__ into __services__:

```php
$serviceContainer = new Container();
$serviceContainer
->registerService('sender', 'dic\Tests\Sender');

$serviceContainer
->registerService('mailer', 'dic\Tests\SmartMailer')
->addArgument($serviceContainer->getService('sender'));

$mailer = $serviceContainer->get('mailer');

/* ... */

class SmartMailer
{
    /** @var  Sender */
    private $sender;

    public function __construct(Sender $sender)
    {
        $this->sender = $sender;
    }
}

class Sender
{
}

```
